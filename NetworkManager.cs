﻿using RiptideNetworking;
using RiptideNetworking.Transports.RudpTransport;
using RiptideNetworking.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Timer = System.Timers.Timer;
using System.Numerics;


//console
using System.Runtime.InteropServices;
using System.Text;

namespace ICDIBasic
{
    public static class NetworkManager
    {

        [StructLayout(LayoutKind.Explicit, Pack = 1)]
        struct h2f
        {
            #region uint Fields union
            [FieldOffset(0)]
            public uint i;
            #endregion

            #region float Field union
            [FieldOffset(0)]
            public float f;
            #endregion
        }

        private static Client client;
        private static bool isRunning;

        private static bool isRoundTripTest;
        private static bool rotationMessageMode;
        private static bool isTestRunning;
        private static int nextReliableTestId = 1;
        private static readonly int testIdAmount = 10000;
        private static List<int> remainingTestIds;
        private static Timer testEndWaitTimer;
        private static int duplicateCount;
        private static Form1 form;

        private static Quaternion quat = Quaternion.Identity;
        public static uint hmdPos = 0;
        public static uint hmdInPlace = 0;


        static float hex2float(uint i)
        {
            h2f h = new h2f();
            h.i = i;            
            return h.f;
        }


        public static void StartNetwork(Form1 formIn)
        {
            form = formIn;            
            /*

            Console.Title = "Client";

            Console.WriteLine("Press\n  1 to start a one-way test\n  2 to start a round-trip test\n  3 to send robo message");
            Console.WriteLine();

            
            rotationMessageMode = false;

            while (true)
            {
                ConsoleKey pressedKey = Console.ReadKey().Key;
                if (pressedKey == ConsoleKey.D1)
                {
                    isRoundTripTest = false;
                    break;
                }
                else if (pressedKey == ConsoleKey.D2)
                {
                    isRoundTripTest = true;
                    break;
                }
                else if (pressedKey == ConsoleKey.D3)
                {
                    rotationMessageMode = true;
                    break;
                }
            }*/

            rotationMessageMode = true;

            //Console.SetCursorPosition(0, Console.CursorTop);

            RiptideLogger.Initialize(Console.WriteLine, true);
            isRunning = true;

            new Thread(new ThreadStart(Loop)).Start();

            /*
            Console.ReadLine();
            isRunning = false;
            Console.ReadLine();
            */
        }

        public static void StopNetwork()
        {
            isRunning = false;
        }

            public static string Little2BigEndian(string str)
        {            
            int len = str.Length;
            StringBuilder sb = new StringBuilder(str);


            for (int i = 0; i < len/2; i++)
            {
                sb[len-2*i-2] = str[2*i];
                sb[len-2*i-1] = str[2*i+1];
            }  

            return sb.ToString();
        }
     
        public static void processMsg(string data, string id)
        {
            if (data.Length == 24)
            {
                data = data.Replace(" ","");
                if (id == "514h")
                {
                    //msg514 = data;                    
                    uint i_w = uint.Parse(Little2BigEndian(data.Substring(0, 8)), System.Globalization.NumberStyles.HexNumber);
                    float f_w = hex2float(i_w);
                    uint i_x = uint.Parse(Little2BigEndian(data.Substring(8, 8)), System.Globalization.NumberStyles.HexNumber);
                    float f_x = hex2float(i_x);
                    quat.W = f_w;
                    quat.X = f_x;     
                }
                else if (id == "515h")
                {
                    //msg515 = data;
                    uint i_y = uint.Parse(Little2BigEndian(data.Substring(0, 8)), System.Globalization.NumberStyles.HexNumber);
                    float f_y = hex2float(i_y);
                    uint i_z = uint.Parse(Little2BigEndian(data.Substring(8, 8)), System.Globalization.NumberStyles.HexNumber);
                    float f_z = hex2float(i_z);
                    quat.Y = f_y;
                    quat.Z = f_z;
                }       
                else if (id == "711h")
                {                   
                    hmdPos = uint.Parse(data.Substring(0, 2), System.Globalization.NumberStyles.HexNumber); 
                    //01 - down; 02 - up; 03 - lowering; 04 - raising;                    
                }    
            } 
            else if (data.Length == 12)
            {
                if (id == "708h")
                {                                       
                    uint hmdFloating1 = uint.Parse(Little2BigEndian(trunc(data.Substring(0,6))), System.Globalization.NumberStyles.HexNumber); 
                    uint hmdFloating2 = uint.Parse(Little2BigEndian(trunc(data.Substring(6,6))), System.Globalization.NumberStyles.HexNumber); 
                    hmdInPlace = (hmdFloating1 + hmdFloating2) / 2;
                    //0 to ~50 -> ~15+: in place                                        
                }
            }
        }

        private static string trunc(string str)
        {
            return  string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }

        private static void Loop()
        {
            client = new Client(new RudpClient(ushort.MaxValue)); // Max value timeout to avoid getting timed out for as long as possible when testing with very high loss rates (if all heartbeat messages are lost during this period of time, it will trigger a disconnection)
            client.Connected += (s, e) => Connected();
            client.Disconnected += (s, e) => Disconnected();
            client.Connect("127.0.0.1:7777");

            while (isRunning)
            {
                client.Tick();
                
                if (rotationMessageMode)
                {
                    SendRotationMessage(quat);                      
                    SendHmdInPlaceMessage(hmdInPlace);
                    SendHmdPosMessage(hmdPos);                        
                }

                //Thread.Sleep(10);
                Thread.Sleep(5);
            }

            client.Disconnect();
            Disconnected();
        }

        private static void Connected()
        {
            Console.WriteLine();
            Console.WriteLine("Press enter to disconnect at any time.");

            if (rotationMessageMode)
            {
                SendRotationMessage(quat);
                Console.WriteLine("rotationMessage sent!");
            }
            else
            {
                client.Send(Message.Create(MessageSendMode.reliable, MessageId.startTest, 25).AddBool(isRoundTripTest).AddInt(testIdAmount));
            }            
        }

        private static void Disconnected()
        {
            if (testEndWaitTimer != null)
                testEndWaitTimer.Stop();    

            if (isTestRunning)
            {
                Console.WriteLine();
                Console.WriteLine($"Cancelled reliability test ({(isRoundTripTest ? "round-trip" : "one-way")}) due to disconnection.");
                Console.WriteLine();
            }

            isTestRunning = false;
        }

        [MessageHandler((ushort)MessageId.startTest)]
        private static void HandleStartTest(Message message)
        {
            if (message.GetBool() != isRoundTripTest || message.GetInt() != testIdAmount)
            {
                Console.WriteLine();
                Console.WriteLine("Test initialization failed! Please try again.");
                return;
            }

            if (!isTestRunning)
                new Thread(new ThreadStart(StartReliabilityTest)).Start(); // StartReliabilityTest blocks the thread it runs on, so we need to put it on a different thread to avoid blocking the receive thread
        }

        private static void StartReliabilityTest()
        {
            isTestRunning = true;
            duplicateCount = 0;

            remainingTestIds = new List<int>(testIdAmount);
            for (int i = 0; i < testIdAmount; i++)
            {
                remainingTestIds.Add(i + 1);
            }

            Console.WriteLine($"Starting reliability test ({(isRoundTripTest ? "round-trip" : "one-way")})!");

            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < testIdAmount; i++)
            {
                if (!isTestRunning)
                    return;

                sw.Restart();
                while (sw.ElapsedMilliseconds < 2)
                {
                    // Wait
                }

                SendTestMessage(nextReliableTestId++);
            }

            testEndWaitTimer = new Timer(20000);
            testEndWaitTimer.Elapsed += (e, s) => ReliabilityTestEnded();
            testEndWaitTimer.AutoReset = false;
            testEndWaitTimer.Start();
        }

        private static void ReliabilityTestEnded()
        {
            Console.WriteLine();

            if (isRoundTripTest)
            {
                Console.WriteLine("Reliability test complete (round-trip):");
                Console.WriteLine($"  Messages sent: {testIdAmount}");
                Console.WriteLine($"  Messages lost: {remainingTestIds.Count}");
                Console.WriteLine($"  Duplicates:    {duplicateCount}");
                Console.WriteLine($"  Latency (RTT): {client.SmoothRTT}ms");
                if (remainingTestIds.Count > 0)
                    Console.WriteLine($"  Test IDs lost: {string.Join(",", remainingTestIds)}");
                if (duplicateCount > 0)
                    Console.WriteLine("\nThis demo sends a reliable message every 2-3 milliseconds during the test, which is a very extreme use case.\n" +
                        "Riptide's duplicate filter has a limited range, and a send rate like 33-50 reliable messages per 100ms will\n" +
                        "max it out fast once combined with any amount of packet loss.\n\n" +
                        "Most duplicates will be filtered out even at high send rates, but any messages that need to be resent due to\n" +
                        "packet loss (and therefore take longer to arrive) are highly likely to be missed by Riptide's filter. If you\n" +
                        "are simulating latency & packet loss with an app like Clumsy, you'll notice that increasing those numbers\n" +
                        "will also increase the number of duplicates that aren't filtered out.\n\n" +
                        "To reduce the amount of duplicate messages that Riptide does NOT manage to filter out, applications should\n" +
                        "send reliable messages at a reasonable rate (unlike this demo). However, applications should also be\n" +
                        "prepared to handle duplicate messages. Riptide can only filter out duplicates based on sequence ID, but if\n" +
                        "(for example) a hacker modifies his client to send a message twice with different sequence IDs and your\n" +
                        "server is not prepared for that, you may end up with issues such as players being spawned multiple times.");
            }
            else
                Console.WriteLine("Reliability test complete (one-way)! See server console for results.");

            Console.WriteLine();
            isTestRunning = false;
        }

        private static void SendTestMessage(int reliableTestId)
        {
            Message message = Message.Create(MessageSendMode.reliable, MessageId.testMessage);
            message.AddInt(reliableTestId);

            client.Send(message);
        }

        [MessageHandler((ushort)MessageId.testMessage)]
        private static void HandleTestMessage(Message message)
        {
            int reliableTestId = message.GetInt();

            lock (remainingTestIds)
            {
                if (!remainingTestIds.Remove(reliableTestId))
                {
                    duplicateCount++;
                    Console.WriteLine($"Duplicate message received (Test ID: {reliableTestId}).");
                }
            }
        }

        [MessageHandler((ushort)MessageId.liftMessage)]
        private static void HandleLiftMessage(Message message)
        {
            bool lift = message.GetBool();
            form.lift(lift);
            Console.WriteLine($"Received Lift Message {lift}.");
        }

        public static void SendRotationMessage(Quaternion msgContent)
        {
            Message message = Message.Create(MessageSendMode.reliable, MessageId.rotationMessage);            
            Quaternion quat = Quaternion.Normalize(msgContent);            

            // adjust for active passive transformation      
            quat = Quaternion.Normalize(Quaternion.Inverse(msgContent));            
            
            // adjust for imu mounting position            
            Quaternion mounting_adjustment = new Quaternion(0.8903567205877573f,0.010726336793253258f,0.45291950152420835f,0.04487517066569299f);
            mounting_adjustment = Quaternion.Normalize(mounting_adjustment);
            quat = mounting_adjustment * quat * Quaternion.Inverse(mounting_adjustment);                 

            //change_to_hmd_coordinate_system
            Quaternion change_bosch_into_opengl = new Quaternion(0.5f,0.5f,-0.5f,-0.5f);
            change_bosch_into_opengl = Quaternion.Inverse(change_bosch_into_opengl);
            quat = change_bosch_into_opengl * quat * Quaternion.Inverse(change_bosch_into_opengl);                     

            message.AddFloat(quat.X);
            message.AddFloat(quat.Y);
            message.AddFloat(quat.Z);
            message.AddFloat(quat.W);            
            client.Send(message);                   
        }

        public static void SendHmdPosMessage(uint msgContent)
        {
            //01 - down; 02 - up; 03 - lowering; 04 - raising;                    
            Message message = Message.Create(MessageSendMode.reliable, MessageId.hmdPosMessage);
            message.AddInt((int)msgContent);                     
            client.Send(message);                   
            //Console.WriteLine($"sending pos msg: {msgContent}");
        }

        public static void SendHmdInPlaceMessage(uint msgContent)
        {
            //0 to ~50 -> ~15+: in place  
            Message message = Message.Create(MessageSendMode.reliable, MessageId.hmdInPlaceMessage);
            message.AddInt((int)msgContent);
            client.Send(message);                   
        }

        public static void SendConnectivityMessage(bool footpedalConnected, bool hapticDeviceConnected, bool hmdConnected)
        {            
            Message message = Message.Create(MessageSendMode.reliable, MessageId.connectivityMessage);
            message.AddBool(footpedalConnected);
            message.AddBool(hapticDeviceConnected);
            message.AddBool(hmdConnected);
            client.Send(message);
        }


        public static Vector3 QuaternionToEuler(Quaternion q)
        {
            Vector3 euler;

            // if the input quaternion is normalized, this is exactly one. Otherwise, this acts as a correction factor for the quaternion's not-normalizedness
            float unit = (q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z) + (q.W * q.W);

            // this will have a magnitude of 0.5 or greater if and only if this is a singularity case
            float test = q.X * q.W - q.Y * q.Z;

            if (test > 0.4995f * unit) // singularity at north pole
            {                
                euler.X = (float) Math.PI / 2f;
                euler.Y = 2f * (float)Math.Atan2(q.Y, q.X);
                euler.Z = 0;
            }
            else if (test < -0.4995f * unit) // singularity at south pole
            {
                euler.X = -(float) Math.PI / 2;
                euler.Y = -2f * (float) Math.Atan2(q.Y, q.X);
                euler.Z = 0;
            }
            else // no singularity - this is the majority of cases
            {
                euler.X = (float) Math.Asin(2f * (q.W * q.X - q.Y * q.Z));
                euler.Y = (float) Math.Atan2(2f * q.W * q.Y + 2f * q.Z * q.X, 1 - 2f * (q.X * q.X + q.Y * q.Y));
                euler.Z = (float) Math.Atan2(2f * q.W * q.Z + 2f * q.X * q.Y, 1 - 2f * (q.Z * q.Z + q.X * q.X));
            }

            // all the math so far has been done in radians. Before returning, we convert to degrees...
            euler *= 180 / (float) Math.PI;

            //...and then ensure the degree values are between 0 and 360
            euler.X %= 360;
            euler.Y %= 360;
            euler.Z %= 360;

            return euler;
        }
    }
    

    public enum MessageId : ushort
    {
        startTest = 1,
        testMessage,
        rotationMessage,
        liftMessage,
        hmdPosMessage,
        hmdInPlaceMessage,
        connectivityMessage
    }
}
